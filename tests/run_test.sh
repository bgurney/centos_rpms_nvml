#!/bin/bash

cat <<EOF>./source/src/test/testconfig.sh
NON_PMEM_FS_DIR=/tmp
TEST_TYPE=medium
TEST_BUILD=nondebug
TEST_FS=non-pmem
TM=1
KEEP_GOING=y
CLEAN_FAILED=y
PMDK_LIB_PATH_NONDEBUG=/usr/lib64
EOF

cd source
if [ ! -f .skip-doc ]
then
	touch .skip-doc
fi

make check
