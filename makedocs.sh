#!/bin/bash

# makedocs.sh: Build man pages tarball for the nvml rpm.  Must be run
# for each rebase.
#
# RHEL 9 does not include pandoc, which is used by pmdk to generate man
# pages.  Run this script on a system that does include pandoc, such as
# fedora or RHEL 8.

# The package is named nvml, but the upstream tarball is pmdk.
TOPDIR=${PWD}
ver=$(rhpkg --release rhel-9 verrel | cut -d- -f -2 | cut -d- -f 2)
source_dir=pmdk-${ver}
tarfile=pmdk-${ver}-man.tar.gz

rhpkg --release rhel-9 prep
pushd $source_dir
BUILD_RPMEM=n make doc
mkdir man
make -C doc install mandir=$PWD/man
cd man
tar czf ${TOPDIR}/${tarfile} .
popd

echo
echo "Don't forget to upload ${tarfile} to the lookaside repo."
echo -e "\trhpkg upload ${tarfile}"
echo "Remember to commit the sources file."
